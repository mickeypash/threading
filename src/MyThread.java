class MyThread implements Runnable {

	private MyTask task;
	private Thread thread;
	private String threadName;

	public MyThread(String name, MyTask task) {
	    threadName = name;
	    this.task = task;
	    System.out.println("Creating " +  threadName );
	}

	public void run() {
		System.out.println("Running " +  threadName );
		task.computation();
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void start() {
		System.out.println("Starting " + threadName);
		if (thread == null) {
			thread = new Thread(this, threadName);
			thread.start();
		}
	}
}
public class Tasker {

	public static void main(String[] args){

		MyTask task1 = new AnswerToLife();
		MyTask task2 = new NameMyFriends();

		MyTask[] tasks = {task1, task2};
		
		int i = 1;
		for (MyTask task: tasks) {
			MyThread thread = new MyThread((i++) + "",task);
			thread.start();
		}
	}
}
